<?php
// Database configuration
$servername = "localhost"; // Change this to your database server hostname
$username = "aysel"; // Change this to your database username
$password = "1"; // Change this to your database password
$database = "testDB"; // Change this to your database name

// Create connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    echo "Connection failed";
}

$sql = "SELECT project_id, project_name  FROM projects";

// Execute query
$result = $conn->query($sql);

// Check if any rows were returned
if ($result->num_rows > 0) {
    // Output data of each row
    echo "<h1>Projects</h1>";
    while ($row = $result->fetch_assoc()) {
        echo "ID: " . $row["project_id"] . " - Project: " . $row["project_name"] . "<br>";
    }
} else {
    echo "No records found";
}

// Close connection
$conn->close();
?>

