import datetime

# Get the current time
current_time = datetime.datetime.now().time()

# Extract the second from the current time
current_second = current_time.second

# Check if the second is even or odd
if current_second % 2 == 0:
    print(f"The current second ({current_second}) is even.")
else:
    print(f"The current second ({current_second}) is odd.")

