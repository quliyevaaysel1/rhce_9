#!/bin/bash
instance_name=$1
TOMCAT_BASE=/usr/local/tomcat
INSTANCE_BASE=$TOMCAT_BASE/instances
CATALINA_HOME=$TOMCAT_BASE/apache-tomcat-10.1.20
CATALINA_BASE=$INSTANCE_BASE/instance$instance_name
export CATALINA_BASE CATALINA_HOME
$CATALINA_HOME/bin/startup.sh

