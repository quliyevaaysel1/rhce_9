<?php
// Database credentials
$dbhost = "localhost";
$dbuser = "aysel";
$dbpass = "1";
$dbname = "phptest";
// Create a connection to the database
$mysqlcon = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

// Check connection
if ($mysqlcon->connect_errno) {
    die("Unable to connect to database: " . $mysqli->connect_error);
}

// Select the database (optional if already specified in connection)
// $mysqli->select_db($dbname);

// Perform a SELECT query
$query = "SELECT name  FROM employees";
$result = $mysqlcon->query($query);

// Check if query was successful
if ($result) {
    // Fetch data and display results
    while ($row = $result->fetch_assoc()) {
        printf("Name: %s<br>", $row['name']);
    }
    // Free result set
    $result->free();
} else {
    echo "Error: " . $mysqli->error;
}

// Close connection
$mysqlcon->close();
?>

